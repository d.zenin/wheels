#pragma once

#include <wheels/test/test_framework.hpp>

namespace wheels::test {

// Thread-safe
bool KeepRunning();

}  // namespace wheels::test
