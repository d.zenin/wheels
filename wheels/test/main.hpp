#pragma once

#include <wheels/test/test.hpp>

namespace wheels::test {

void RunTestsMain(const TestList& tests, int argc, const char** argv);

}  // namespace wheels::test
